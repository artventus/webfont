# Gulp Webfont workflow

This project contains a [Gulp](http://gulpjs.com/) workflow for generating a webfont from svg icons.

I took this quest upon myself to create the best Gulp webfont workflow known to mankind. I added some utilities that wouldn't be necessary but hey, looks good.

## Prepare

We want to autohint our fonts so we need [ttfautohint](https://www.freetype.org/ttfautohint/) to be installed on our system. This can be done with [Homebrew](http://brew.sh/).

Install Homebrew with:
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Then install ttfautohint with Homebrew.
```
brew install ttfautohint
```

## Folder structure

The current folder structure is setup to the way we work at [Artventus](http://artventus.nl/). And I made it ready to use in my current projects that use Gulp.

* src/ - *Where all the project source files live*
	* assets/ - *Contains the project assets e.g. styles, images, etc.*
		* fonts/ - *This is where the generated fonts will be*
		* icons/ - *The svg icons of which the webfont will be created*
		* styles/ - *Contains the webfont styles and template*

## Using the webfont

This workflow generates the webfonts in `ttf`, `eot` and `woff` format. It alse generates a stylesheet `.scss` to be imported into your projects css. The generated stylesheet adds the font faces and icon classes. Based on the [BEM](https://css-tricks.com/bem-101/) methodology the main class is `.icon` and uses modifiers to display the correct icon.

```
<span class="icon icon--{{modifier_class}}"><span>
```