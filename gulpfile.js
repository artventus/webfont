'use strict';

/**
 * Load required packages
 */
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();



/**
 * Config
 */
var pkg = require('./package.json');
var config = {
	src: './src',
};



/**
 * Webfont
 */
gulp.task('webfont', function() {
	var fontName = 'icon';

	return gulp.src([config.src + '/assets/icons/**/*.svg'])
		.pipe($.iconfontCss({
			fontName: fontName,
			path: config.src + '/assets/styles/objects/_icon.template.scss', // Custom stylesheet template
			targetPath: '../../styles/objects/_icon.scss', // Target stylesheet relative to gulp.dest
			fontPath: '../fonts/icons/', // Relative font path from stylesheet
			cssClass: 'icon' // Default
    	}))
    	.pipe($.iconfont({
			fontName: fontName,
			autohint: true, // (Optional) Requires `ttfautohint` to be installed on your system
			fontHeight: 512, // Default MAX(icons.height)
			normalize: true, // Default false
			timestamp: Math.round(Date.now()/1000), // recommended to get consistent builds when watching files
			log: function(){} // Remove default logging message
		})).on('glyphs', function(glyphs, options) {
			$.util.log($.util.colors.cyan('Webfont'), $.util.colors.green(options.fontName), 'created with', $.util.colors.magenta(glyphs.length), 'glyphs');
		})
		.pipe(gulp.dest(config.src + '/assets/fonts/icon'))
		.pipe($.size({ title: 'Webfont' }));
});



/**
 * Default task
 */
gulp.task('default', ['webfont']);